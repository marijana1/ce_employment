#!/usr/bin/env python

import time
import numpy as np
import pandas as pd
import pymrio

from pymrio.tools.iomath import calc_A, calc_L

from utils.io import downscale_Z, downscale_Y, iot_exp

import warnings
# TODO not a good idea, waiting for a pymrio update
warnings.filterwarnings("ignore")

init_start_time = time.time()

EORA_PATH = "./data/eora"

# Load eora data
# TODO this has to be loaded before calling the function at a higher level
print('{:<30}'.format('Load EORA data'), end='\t')
io = pymrio.parse_eora26(year=2013, path=EORA_PATH)
io.calc_all()
print('@ {:0.2f}'.format(time.time() - init_start_time))


def compute_city(country, city, mid_method, verbose=False):
    if verbose: print('computing {}/{} with method: {}'.format(country, city, mid_method)); start_time = time.time()
    empl_data = "./data/empl_{}.csv".format(city[0:3])

    # Prepare the tables we care about
    Z = io.Z.loc[country, country]
    Y = io.Y.loc[country, country].sum(axis=1)

    # Load prepped bottom up data sheet
    if verbose: print('{:<30}'.format('\t(1) bottom up data'), end="\t"); start_time = time.time()

    # Note that this currently assumes mappings and scaling data are already part of the
    # dataset. We want to generalize this in the near future.
    brussels = pd.read_csv(empl_data)
    region_empl = brussels.copy()
    brussels.set_index("eora_name", inplace=True)

    # OLD aggregattion Method: works because of input file contains aggregated values at city level
    region_empl = region_empl.drop_duplicates(['eora_name']).set_index("eora_name")
    empl_p = region_empl["city_ppn"]

    re_ce_empl = brussels.copy()
    re_ce_empl["io_code"] = re_ce_empl["newempcode"]
    re_ce_empl["empl"] = re_ce_empl["city_employment"]
    re_ce_empl["empl_p"] = re_ce_empl["city_sector_ppn"]

    print('@ {:0.2f}'.format(time.time() - start_time))

    # MIDs
    if verbose: print('{:<30}'.format('\t(2) MID'), end="\t"); start_time = time.time()
    inx = ['Raw material inputs' in i[0] and i[1] == 'Total' for i in io.Q.D_imp.index]

    imp = io.Q.D_imp.loc[inx, country].sum()
    cba = io.Q.D_cba.loc[inx, country].sum()
    pba = io.Q.D_pba.loc[inx, country].sum()

    mid = pd.concat([imp, cba, pba], axis=1, keys=["imp", "cba", "pba"])

    # Consumption based
    mid["cmid"] = mid["imp"] / (mid[mid_method] + mid["imp"])
    mid.loc[mid["cmid"] == float('inf'), 'cmid'] = 0

    # Take the inverse (high dependency is bad)
    mid["mid"] = 1 - mid["cmid"]

    # Apply 0 final demand to mining and quarrying as it is linear industry
    mid.loc["Mining and Quarrying"] = 0

    # Expand to our sectors
    mid_exp = re_ce_empl.join(mid, how="left")
    mid_exp.set_index("io_code", inplace=True)
    mid_exp = mid_exp["mid"]

    print('@ {:0.2f}'.format(time.time() - start_time))

    # IO
    if verbose: print('{:<30}'.format('\t(3) Downscale IO'), end="\t"); start_time = time.time()

    # downscaled IO tables
    Z = downscale_Z(Z, empl_p)
    Y = downscale_Y(Y, empl_p)

    # expand to detailed sectors
    Z, Y = iot_exp(Z, Y, re_ce_empl)

    print('@ {:0.2f}'.format(time.time() - start_time))

    if verbose: print('{:<30}'.format('\t(4) Index IO'), end="\t"); start_time = time.time()
    # easy index access
    midx = re_ce_empl["io_code"]

    # Boolean vectors
    core_mask = midx.str.contains(".C", regex=False)
    enabling_mask = midx.str.contains(".E", regex=False)
    indirectly_mask = midx.str.contains(".I", regex=False)
    print('@ {:0.2f}'.format(time.time() - start_time))

    if verbose: print('{:<30}'.format('\t(5) A, L matrix'), end="\t"); start_time = time.time()
    # Creating A, L matrices

    # Sum over the rows
    output_x = Z.sum(axis=1) + Y

    A = calc_A(Z, output_x)
    L = calc_L(A)
    print('@ {:0.2f}'.format(time.time() - start_time))

    if verbose: print('{:<30}'.format('\t(6) Mask'), end="\t"); start_time = time.time()
    # Calculate employent intensity vector
    emp_city = re_ce_empl["empl"]
    emp_city.index = L.index

    # Cjob calc strategies
    results = [emp_city]

    # Modify Y (in/in)

    # Set a dummy df based on the original empl data and zero out all the non-core
    # sectors. This allows us to easily copy it back in later on using just a sum.
    emp_core = emp_city.copy()
    emp_core.index = L.index
    emp_core.loc[~core_mask.values] = 0

    print('@ {:0.2f}'.format(time.time() - start_time))
    if verbose: print('{:<30}'.format('\t(7) Apply Mask'), end="\t"); start_time = time.time()
    # Generate a Y mask. Sum over the inputs (core prop). This is equal to:
    #
    #               core    enabling    indirectly
    # core          -       -           0.x
    # enabling      0.x     -           -
    # indirectly    -       -           -
    Y_mask_core = Z.apply(lambda row: 0, axis=1)  # Not used
    Y_mask_core[~core_mask.values] = 0

    Y_mask_enabling = Z.apply(lambda row: row[core_mask.values].sum() / row.sum(), axis=1).fillna(0)
    Y_mask_enabling[~enabling_mask.values] = 0

    # Indirectly sectors have circular final demand to the extent they use core or enabling products AND local materials
    Y_mask_indirectly = Z.apply(lambda col: (col[core_mask.values].sum() + col[enabling_mask.values].sum()) / col.sum(), axis=0).fillna(0)
    Y_mask_indirectly[~indirectly_mask.values] = 0
    Y_mask_indirectly_mid = Y_mask_indirectly * mid_exp

    # Overlay to make a single mask
    Y_mask = Y_mask_core + Y_mask_enabling + Y_mask_indirectly_mid
    # Y_mask.loc[Y_mask.index.str.contains(".I.", regex=False)] = 0
    dx = L @ (Y * Y_mask)

    print('@ {:0.2f}'.format(time.time() - start_time))
    if verbose: print('{:<30}'.format('\t(8) Results'), end="\t"); start_time = time.time()

    # Divide dx over original x to get the proportions
    y_mp = dx / output_x

    emp_new = y_mp * emp_city

    emp_new.name = "cjobs"
    results.append(emp_new)

    print('@ {:0.2f}'.format(time.time() - start_time))
    # Return result
    return pd.concat(results, axis=1)


def main():
    verbose = True
    if verbose: print("\n--------VERBOSE--------\n")
    mid_method = 'cba'

    for country in ['BRA', 'BEL']:
        for city in ['recife', 'brussels']:

            result = compute_city(country, city, mid_method, verbose=verbose)
            results_path = "./tmp/{}_{}_out_{}.csv".format(country, city, mid_method)
            if verbose:
                print('='*30)
                print(result.head())
                print('-' * 30)
                print(result.sum(axis=0))
                print('=' * 30)
                print("{}-{} - {:0.2%} Circular".format(
                    country, city.title(), result.cjobs.sum() / result.empl.sum()))
                print('=' * 30)
                print("\n\n")
            result.to_csv(results_path)

    if verbose: print('\nFINAL TIME: {:0.2f}'.format(time.time() - init_start_time))

if __name__ == "__main__":
    main()
